#
# Author: Gina Jiménez
# Maintainer(s): GJ, OE, MS, AL, AF
# License: (c) Data Cívica 2020, GPL v2 or newer
#
# -----------------------------------------------
# blog_homicidioscasa_mujeres/grafs/src/grafs

require(pacman)
p_load(tidyverse, here, janitor)

files <- list(sinais = here("clean-data/output/sinais-all.rds"),
              pob= here("clean-data/output/pob-ent.rds"),
              pob2="/Volumes/GoogleDrive/Mi unidad/Data Cívica (Drive)/Libreria-Datos/Poblaciones/Estatales/Edicion-2019/pob_mit_proyecciones.csv")
#### Tema ####
tema <- theme_minimal() +
  theme(plot.title = element_text(size = 16, family = "Barlow Condensed", hjust = 0.5, face = "bold"),
        plot.subtitle = element_text(size = 12, family = "Barlow Condensed", hjust = 0.5),
        plot.caption = element_text(size = 10, family = "Barlow Condensed", hjust = 0, face = "italic"),
        axis.text = element_text(size = 10, family = "Barlow Condensed", face = "bold"),
        axis.title = element_text(size = 12, family = "Barlow Condensed"),
        legend.text = element_text(size = 10, family = "Barlow Condensed", hjust = 0.5),
        legend.title = element_text(size = 10, family = "Barlow Condensed", hjust = 0.5),
        strip.text = element_text(size = 12, face = "bold", family = "Barlow Condensed"))

dos_colores <- c("#43a2ca", "#dd1c77")
cuatro_colores <- c("#361134", "#B0228C", "#EA3788", "#E56B70")

firstup <- function(x) {
  substr(x, 1, 1) <- toupper(substr(x, 1, 1))
  x
}
#### Limpiar población ####
pob_muj <- readRDS(files$pob)%>%
           filter(year<2021)%>%
           filter(cve_ent!="00")%>%
           group_by(year, sexo, cve_ent)%>%
           summarize(pob=sum(pob_ent, na.rm=T))%>%
           filter(sexo=="Mujeres")


#### Fiebre nacional en casa o en vivienda ####
data <- readRDS(files$sinais)%>%
        mutate(age_group = case_when(edad < 12 ~ 1,
                                     edad %in% 12:19 ~ 2,
                                     edad %in% 20:29 ~ 3,
                                     edad %in% 30:39 ~ 4,
                                     edad %in% 40:49 ~ 5,
                                     edad %in% 50:59 ~ 6,
                                     edad > 59 ~ 7), 
               age_group=factor(age_group, levels=c(1:7),
                          labels=c("Menores de 12 años",
                                   "De 12 a 19 años",
                                   "De 20 a 29 años",
                                   "De 30 a 39 años",
                                   "De 40 a 49 años",
                                   "De 50 a 59 años",
                                   "Más de 59 años")),
               edo_civil = case_when(edo_civil == "Casado" ~ "Casada",
                                     edo_civil == "No aplica" ~ "No especificado",
                                     edo_civil == "Separado o Divorciado" ~ "Separada o Divorciada",
                                     edo_civil == "Soltero" ~ "Soltera",
                                     edo_civil == "Unión Libre" ~ "Union libre",
                                     edo_civil == "Viudo" ~ "Viuda"))


tempo <- pob_muj%>%
         group_by(year)%>%
         summarize(pob=sum(pob, na.rm=T))

data%>%
  filter(lugar=="Vivienda" | lugar=="Vía pública")%>%
  filter(sexo=="Mujer")%>%
  group_by(lugar, year)%>%
  summarize(total=n())%>%
  ungroup()%>%
  left_join(., tempo)%>%
  mutate(tasa=total/pob*100000)%>%
  ggplot()+
  geom_line(aes(x = as.integer(year), y=tasa, color=lugar), size=1.5) +
  geom_point(aes(x = as.integer(year), y=tasa, color=lugar), size=3) +
  scale_color_manual(values = cuatro_colores) +
  labs(title = "Tasa de asesinatos de mujeres en México",
       subtitle= "Según el lugar del asesinato",
       x = "Año", y = "Tasa",
       caption = "Fuente: Defunciones de INEGI", color="") +
  tema +
  theme(legend.position = "top")
ggsave(here("grafs/output/fiebre_nacional.jpg"),
       width = 14, height = 10)
ggsave(here("grafs/output/fiebre_nacional.svg"),
       width = 14, height = 10)


#### Fiebre nacional en casa o en vivienda con arma y otro ####
data%>%
  filter(lugar=="Vivienda" | lugar=="Vía pública")%>%
  filter(sexo=="Mujer")%>%
  mutate(causa2=ifelse(causa_hom=="Arma de fuego", "Arma de fuego", "Otro"))%>%
  group_by(lugar, year, causa2)%>%
  summarize(total=n())%>%
  ungroup()%>%
  left_join(., tempo)%>%
  mutate(tasa=total/pob*100000)%>%
  ggplot()+
  geom_line(aes(x = as.integer(year), y=tasa, color=causa2), size=1.5) +
  geom_point(aes(x = as.integer(year), y=tasa, color=causa2), size=3) +
  scale_color_manual(values = cuatro_colores) +
  labs(title = "Tasa de asesinatos de mujeres en México",
       subtitle= "Según el lugar del asesinato y si fue con arma de fuego",
       x = "Año", y = "Tasa",
       caption = "Fuente: Defunciones de INEGI", color="") +
  tema +
  theme(legend.position = "top")+
  facet_wrap(~lugar)
ggsave(here("grafs/output/fiebre_armas.svg"),
       width = 14, height = 10)
ggsave(files$fiebre_nacional, width = 14, height = 10)

#### Area de ocurrencia en vivienda y vía pública  ####
aver <- data%>%
  filter(lugar=="Vivienda" | lugar=="Vía pública")%>%
  filter(sexo=="Mujer")%>%
  group_by(year, lugar, causa_hom)%>%
  summarize(total=n())%>%
  ungroup()%>%
  group_by(year, lugar)%>%
  mutate(den=sum(total, na.rm=T))%>%
  ungroup()%>%
  mutate(per=total/den*100)%>%
  select(year, lugar, causa_hom,per)%>%
  mutate(causa_hom=gsub(" ", "_", causa_hom))%>%
  pivot_wider(., names_from=causa_hom, values_from=per)

aver[is.na(aver)] <- 0

aver <- pivot_longer(aver, 
                     3:9, names_to="causa2", values_to="per")%>%
        mutate(causa2=gsub("_", " ", causa2))
ggplot(aver)+
  geom_area(aes(x = as.integer(year), y=per, fill=causa2)) +
  labs(title = "Modo de ocurrencia de los asesinatos de mujeres en México",
       subtitle= "Según el  lugar del asesinato",
       x = "Año", y = "Porcentaje",
       caption = "Fuente: Defunciones de INEGI", fill="") +
  tema +
  theme(legend.position = "top")+
  facet_wrap(~lugar)
ggsave(here("grafs/output/modo_mujeres.svg"),
       width = 14, height = 10)

#### Area de ocurrencia en vía pública en los estados  ####
aver <- data%>%
  filter(lugar=="Vía pública")%>%
  filter(sexo=="Mujer")%>%
  group_by(year, nom_ent, causa_hom)%>%
  summarize(total=n())%>%
  ungroup()%>%
  group_by(year, nom_ent)%>%
  mutate(den=sum(total, na.rm=T))%>%
  ungroup()%>%
  mutate(per=total/den*100)%>%
  filter(!is.na(nom_ent))%>%
  select(year, nom_ent, causa_hom, per)%>%
  mutate(causa_hom=gsub(" ", "_", causa_hom))%>%
  pivot_wider(., names_from=causa_hom, values_from=per)

aver[is.na(aver)] <- 0

aver <- pivot_longer(aver, 
                     3:9, names_to="causa2", values_to="per")%>%
        mutate(causa2=gsub("_", " ", causa2),
               nom_ent=gsub(" de Ignacio de la Llave", "", nom_ent),
               nom_ent=gsub(" de Ocampo", "", nom_ent),
               nom_ent=gsub("Distrito Federal", "Ciudad de México", nom_ent),
               nom_ent=gsub(" de Zaragoza", "", nom_ent))
ggplot(aver)+
  geom_area(aes(x = as.integer(year), y=per, fill=causa2)) +
  labs(title = "Modo de ocurrencia de los asesinatos de mujeres en vía pública",
       subtitle= "Según el estado del asesinato",
       x = "Año", y = "Porcentaje",
       caption = "Fuente: Defunciones de INEGI", fill="") +
  tema +
  theme(legend.position = "top")+
  facet_wrap(~nom_ent)
ggsave(here("grafs/output/modo__edos_vía_publica.svg"),width = 14, height = 10)

#### Area de ocurrencia en vía pública en los estados  ####
aver <- data%>%
  filter(lugar=="Vivienda")%>%
  filter(sexo=="Mujer")%>%
  group_by(year, nom_ent, causa_hom)%>%
  summarize(total=n())%>%
  ungroup()%>%
  group_by(year, nom_ent)%>%
  mutate(den=sum(total, na.rm=T))%>%
  ungroup()%>%
  mutate(per=total/den*100)%>%
  filter(!is.na(nom_ent))%>%
  select(year, nom_ent, causa_hom, per)%>%
  mutate(causa_hom=gsub(" ", "_", causa_hom))%>%
  pivot_wider(., names_from=causa_hom, values_from=per)

aver[is.na(aver)] <- 0


aver <- pivot_longer(aver, 
                     3:9, names_to="causa2", values_to="per")%>%
  mutate(causa2=gsub("_", " ", causa2),
         nom_ent=gsub(" de Ignacio de la Llave", "", nom_ent),
         nom_ent=gsub(" de Ocampo", "", nom_ent),
         nom_ent=gsub("Distrito Federal", "Ciudad de México", nom_ent),
         nom_ent=gsub(" de Zaragoza", "", nom_ent))
ggplot(aver)+
  geom_area(aes(x = as.integer(year), y=per, fill=causa2)) +
  labs(title = "Modo de ocurrencia de los asesinatos de mujeres en vivienda",
       subtitle= "Según el estado del asesinato",
       x = "Año", y = "Porcentaje",
       caption = "Fuente: Defunciones de INEGI", fill="") +
  tema +
  theme(legend.position = "top")+
  facet_wrap(~nom_ent)
ggsave(here("grafs/output/modo__edos_vivienda.svg"),
       width = 14, height = 10)

#### Fiebre por edad ####
pob <- read.csv(files$pob2)%>%
       rename(year=ano)%>%
       filter(year>1999 & year<2021)%>%
       filter(sexo=="Mujeres")%>%
       filter(cve_geo==0)%>%
       mutate(age_group = case_when(edad < 12 ~ 1,
                                    edad %in% 12:19 ~ 2,
                                    edad %in% 20:29 ~ 3,
                                    edad %in% 30:39 ~ 4,
                                    edad %in% 40:49 ~ 5,
                                    edad %in% 50:59 ~ 6,
                                    edad > 59 ~ 7), 
              age_group=factor(age_group, levels=c(1:7),
                               labels=c("Menores de 12 años",
                                        "De 12 a 19 años",
                                        "De 20 a 29 años",
                                        "De 30 a 39 años",
                                        "De 40 a 49 años",
                                        "De 50 a 59 años",
                                        "Más de 59 años")))%>%
        group_by(year, age_group)%>%
        summarize(pob=sum(poblacion, na.rm=T))
  
data%>%
  filter(lugar=="Vivienda" | lugar=="Vía pública")%>%
  filter(sexo=="Mujer")%>%
  filter(!is.na(age_group))%>%
  group_by(lugar, year, age_group)%>%
  summarize(total=n())%>%
  ungroup()%>%
  left_join(., tempo)%>%
  mutate(tasa=total/pob*100000)%>%
  ggplot()+
  geom_line(aes(x = as.integer(year), y=tasa, color=age_group), size=1.5) +
  geom_point(aes(x = as.integer(year), y=tasa, color=age_group), size=3) +
  labs(title = "Tasa de asesinatos de mujeres en México",
       subtitle= "Según rango de edad y lugar del asesinato",
       x = "Año", y = "Tasa",
       caption = "Fuente: Defunciones de INEGI", color="") +
  tema +
  theme(legend.position = "top")+
  facet_wrap(~lugar)
ggsave(here("grafs/output/fiebre_nacional_edad.svg"),
       width = 14, height = 10)

#### Fiebre por estado civil ####
pob <- read.csv(here("import/input/edo_civil.csv"))%>%
       clean_names()
pob$x <- NULL
pob <- pivot_longer(pob, 2:9, names_to="edo_civil",
                    values_to="total")%>%
       mutate(total=gsub(",", "", total),
              total=as.numeric(total),
              age_group = case_when(age_group == "De 12 a 14 años" ~ 2,
                                    age_group == "De 15 a 19 años" ~ 2,
                                    age_group == "De 20 a 24 años" ~ 3,
                                    age_group == "De 25 a 29 años" ~ 3,
                                    age_group == "De 30 a 34 años" ~ 4,
                                    age_group == "De 35 a 39 años" ~ 4,
                                    age_group == "De 40 a 44 años" ~ 5,
                                    age_group == "De 45 a 49 años" ~ 5,
                                    age_group == "De 50 a 54 años" ~ 6,
                                    age_group == "De 55 a 59 años" ~ 6,
                                    age_group == "De 60 a 64 años" ~ 7,
                                    age_group == "De 65 a 69 años" ~ 7,
                                    age_group == "De 70 a 74 años" ~ 7,
                                    age_group == "De 75 a 79 años" ~ 7,
                                    age_group == "De 12 a 14 años" ~ 7,
                                    age_group == "De 80 a 84 años" ~ 7,
                                    age_group == "85 años y más" ~ 7), 
              age_group=factor(age_group, levels=c(1:7),
                               labels=c("Menores de 12 años",
                                        "De 12 a 19 años",
                                        "De 20 a 29 años",
                                        "De 30 a 39 años",
                                        "De 40 a 49 años",
                                        "De 50 a 59 años",
                                        "Más de 59 años")))
edo_civil <- filter(pob, edo_civil!="total")%>%
             mutate(edo_civil=sub("divorciada_o", "Separada o Divorciada", edo_civil),
                    edo_civil=sub("separada_o", "Separada o Divorciada", edo_civil), 
                    edo_civil=gsub("_o", "", edo_civil),
                    edo_civil=gsub("_", " ", edo_civil),
                    edo_civil=firstup(edo_civil))%>%
             group_by(edo_civil)%>%
             summarize(pob=sum(total, na.rm=T))%>%
             ungroup()

data%>%
filter(lugar=="Vivienda" | lugar=="Vía pública")%>%
filter(sexo=="Mujer")%>%
filter(!is.na(edo_civil))%>%
filter(edo_civil!="No especificado")%>%
filter(year>2017)%>%
group_by(lugar, edo_civil, year)%>%
summarize(total=n())%>%
left_join(., edo_civil)%>%
mutate(tasa=total/pob*100000)%>%
ggplot()+
geom_line(aes(x = as.integer(year), y=tasa, color=edo_civil), size=1.5) +
labs(title = "Tasa de asesinatos de mujeres en México",
       subtitle= "Según rango de edad y lugar del asesinato",
       x = "Año", y = "Tasa",
       caption = "Fuente: Defunciones de INEGI", color="") +
tema +
theme(legend.position = "top")+
facet_wrap(~lugar)



  



             
  
  